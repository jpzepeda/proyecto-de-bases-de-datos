<?php
function generateSearchForm(array $humanReadable, array $searchParameters, string $thingLookingFor, $stop=-1)
{
    $counter=0;
    setcookie("table", $thingLookingFor, time()+60*60*24*365, "/");
    echo "<div class=\"col-md-7 col-lg-8\"> <h4 class=\"mb-3\"> <span data-feather=\"search\"></span> $thingLookingFor</h4> <form action = \"DisplayTable.php \" method=\"get\" card p-2>";
    echo "<div class=\"row g-3\">";
    foreach ($searchParameters as $parameter) {
        if ($counter==$stop) {
            break;
        }
        echo <<<IAMSOMEDUMMYTEXT
<br>

<div class="col-sm-6">
  <label for="$humanReadable[$counter]" class="form-label">Ingresa el $humanReadable[$counter] de el $thingLookingFor a buscar</label>
  <input type="text" name="$parameter" size="48" id="$humanReadable[$counter]" class="form-control">
</div>
IAMSOMEDUMMYTEXT;
        $counter++;
    }
    echo "</div>";
      echo "<br><input type=\"submit\" value=\"Buscar\" class=\"btn btn-secondary\"><input type=\"reset\" value=\"Borrar\" class=\"btn btn-secondary\">";
    echo "</form>";
}
function generateSelectSql(string $tableName, array $tableRows):string
{
    $numberOfElements = count($tableRows);
    $selectsql="select count(*) from $tableName ";
    $tablesToJoin = array_keys($tableRows);
    $foreigKeys = array_values($tableRows);
    for ($i=0;$i<$numberOfElements;$i++) {
        $selectsql.="inner join {$tablesToJoin[$i]} using ({$foreigKeys[$i]}) ";
    }
    return $selectsql;
}
function generateRowsToShow(string $tableName, array $RowsFromVariousTables):string
{
    $numberOfElements = count($RowsFromVariousTables);
    $rowsToShow="";
    $tablesToJoin = array_values($RowsFromVariousTables);
    $wantedRows = array_keys($RowsFromVariousTables);
    for ($i=0;$i<$numberOfElements;$i++) {
        $rowsToShow.="{$tablesToJoin[$i]}.{$wantedRows[$i]}";
        if ($i<($numberOfElements-1)) {
            $rowsToShow.=",";
        }
    }
    return $rowsToShow;
}
//I'm kidnapped in your eyes baby
function printRowsOnTable(array $Rows, array $RowsToPrint, $stop=-1, string $serverDomain, string $tableName, string $currentPK, string $pkName, array $specialSymbols=[])
{
    $howMany=count($RowsToPrint);
    if (($cheackSymbols= count($specialSymbols))>0 && $cheackSymbols!=($howMany-$stop)) {
        return -1;
    }
    echo "<tr>";
    $i=0;
    /* echo "<th> <a href=\"http://".$serverDomain."/drop.php?table=$tableName&pk=$currentPK&pkname=$pkName\">&#129312;</a> <a href=\"http://".$serverDomain."/edit.php?table=$tableName&pk=$currentPK&pkname=$pkName\">&#129320;</a> </th>"; */
    echo "<th> <a class=\"nav-link\" href=\"/drop.php?table=$tableName&pk=$currentPK&pkname=$pkName\"> <span data-feather=\"delete\"></span> </a> <a class=\"nav-link\" href=\"/edit.php?table=$tableName&pk=$currentPK&pkname=$pkName\"> <span data-feather=\"edit\"></span> </a> </th>";
    foreach ($RowsToPrint as $Row) {
        if ($i==$stop) {
            ?>
        <td><?php echo $Rows[$Row];
            ; ?>
        <?php
        echo $specialSymbols[$i-$stop];
        } elseif ($i>$stop && $i<$howMany) {
            ?>
        <?php echo $Rows[$Row];
            ; ?>
        <?php
        echo $specialSymbols[$i-$stop];
        } elseif ($i==$howMany) {
            ?>
        <?php echo $Rows[$Row];
            ; ?></td>
        <?php
        echo $specialSymbols[$i-$stop];
        } else {
            ?>
        <td><?php echo $Rows[$Row];
            ; ?> </td>
        <?php
        }
        $i++;
    }
    echo "</tr>";
}
function generateHumanRedable(array $humanRedableRows)
{
    echo "<thead>";
    echo "<tr>";
    echo "<th>Acciones</th>";
    foreach ($humanRedableRows as $current) {
        echo "<th>{$current}</th>";
    }
    echo "</tr>";
    echo "</thead>";
}
function generateInsertForm(array $previwText, $RowsFromVariousTables, PDO $db, array $humanReadable, array $searchParameters, string $form, string $thingLookingFor, $indexToList, $indexToIgnore, $isDate)
{
    $counter=0;
    echo "<div class=\"col-md-7 col-lg-8\"> <h4 class=\"mb-3\"> <span data-feather=\"search\"></span> $thingLookingFor</h4><form action = \"$form \" method=\"post\" card p-2>";
    echo "<div class=\"row g-3\">";
    foreach ($searchParameters as $parameter) {
        $preFilledText = $previwText[$parameter];
        if ($indexToIgnore[$counter]!=1) {
            if ($indexToList[$counter]==0 && $isDate[$counter]==0) {
                echo <<<IAMSOMEDUMMYTEXT
<div class="col-sm-6">
<label for="$humanReadable[$counter]" class="form-label">Ingresa el $humanReadable[$counter] de el $thingLookingFor a ingresar</label>
<input type="text" name="$parameter" size="48" required=true value="$preFilledText" id="$humanReadable[$counter]" class="form-control">
</div>
IAMSOMEDUMMYTEXT;
            } elseif ($isDate[$counter]==1) {
                echo <<<IAMSOMEDUMMYTEXT
<div class="col-sm-6">
  <label for="$humanReadable[$counter]" class="form-label">Ingresa el $humanReadable[$counter] de el $thingLookingFor a ingresar</label>
<input type="date" name="$parameter" size="48" required=true value="$preFilledText" id="$humanReadable[$counter]" class="form-control">
</div>
IAMSOMEDUMMYTEXT;
            } else {
                echo <<<IAMSOMEDUMMYTEXT
<div class="col-sm-6">
  <label for="$humanReadable[$counter]" class="form-label">Ingresa el $humanReadable[$counter] de el $thingLookingFor a buscar</label>
IAMSOMEDUMMYTEXT;
                htmlSelectFromSqlRow($db, $RowsFromVariousTables[$parameter], $parameter, $db);
                echo "</div>";
            }
        }
        $counter++;
    }
    echo "</div>";
    echo "<br><input type=\"submit\" value=\"Ingresar\" class=\"btn btn-secondary\"><input type=\"reset\" value=\"Borrar\" class=\"btn btn-secondary\">";
    echo "</center>\n</form>";
}
  function htmlSelectFromSqlRow(PDO $db, $tableName, string $rowName, PDO $bd)
  {
      $selectsql = "select $tableName.$rowName from $tableName;";
      $stmt = $db->query($selectsql);
      $columnToPrint=[];
      $counter=0;
      while ($row = $stmt->fetch()) {
          $columnToPrint[$counter]=$row[$rowName];
          $counter++;
      }
      echo "<select id=\"$rowName\" name=\"$rowName\" required=true class=\"form-control\">";
      echo "<option value=\"\" selected>Please choose</option>";
      foreach ($columnToPrint as $selectElement) {
          echo "<option value=\"$selectElement\">$selectElement</option>";
      }
      echo "</select>";
  }
  function genHeader($serverDomain, $serverPort)
  {
      echo <<<IAMSOMEDUMMYTEXT
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Permissions Dashboard">
    <meta name="author" content="Content by Juan Pablo Nuño Zepeda, styleshet by Bootstrap contributors">
    <meta name="generator" content="Hugo 0.83.1">
    <title>School Permissions system</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>


    <!-- Custom styles for this template -->
    <link href="dashboard.css" rel="stylesheet">

  </head>
  <body>

<header class="navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0 shadow">
  <a class="navbar-brand col-md-3 col-lg-3 me-0 px-2" href="/">
    <img src="logo.png" alt="" width="60" height="50" class="d-inline-block align-text-center">
    Renta PC
  </a>
  <button class="navbar-toggler position-absolute d-md-none collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#sidebarMenu" aria-controls="sidebarMenu" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <ul class="navbar-nav px-3">
    <li class="nav-item text-nowrap">
      <a class="nav-link" href="/">Log out</a>
    </li>
  </ul>
</header>

<div class="container-fluid">
  <div class="row">
    <nav id="sidebarMenu" class="col-md-3 col-lg-2 d-md-block bg-light sidebar collapse">
      <div class="position-sticky pt-3">
        <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
          <a class="link-secondary" href="#" aria-label="Add a new report">
          </a>
        </h6>
        <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
          <span>Buscar</span>
          <a class="link-secondary" href="#" aria-label="Add a new report">
            <span data-feather="search"></span>
          </a>
        </h6>
        <ul class="nav flex-column">
          <li class="nav-item">
            <a class="nav-link" href="/Search.php?table=modelos">
              <span data-feather="cpu"></span>
            Modelos
            </a>
          </li>

          <li class="nav-item">
            <a class="nav-link" href="Search.php?table=equipos">
              <span data-feather="monitor"></span>
              Equipos
            </a>
          </li>

          <li class="nav-item">
            <a class="nav-link" href="/Search.php?table=personas">
              <span data-feather="users"></span>
              Personas
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/Search.php?table=prestamos">
              <span data-feather="dollar-sign"></span>
              Prestamos
            </a>
          </li>
        </ul>
        <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
          <span>Ingresar</span>
          <a class="link-secondary" href="#" aria-label="Add a new report">
            <span data-feather="edit-2"></span>
          </a>
        </h6>
        <ul class="nav flex-column">
          <li class="nav-item">
            <a class="nav-link" href="/Insert.php?table=modelos">
              <span data-feather="cpu"></span>
            Modelos
            </a>
          </li>

          <li class="nav-item">
            <a class="nav-link" href="Insert.php?table=equipos">
              <span data-feather="monitor"></span>
              Equipos
            </a>
          </li>

          <li class="nav-item">
            <a class="nav-link" href="/Insert.php?table=personas">
              <span data-feather="users"></span>
              Personas
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/Insert.php?table=prestamos">
              <span data-feather="dollar-sign"></span>
              Prestamos
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/Insert.php?table=penalizacion">
              <span data-feather="alert-triangle"></span>
              Penalizaciones
            </a>
          </li>
        </ul>
    </nav>

    <main class="col-md-9 ms-sm-auto col-lg-10 px-md-4">
      <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">Sistema de administación de prestamos de Renta PC</h1>
        <div class="btn-toolbar mb-2 mb-md-0">
          <div class="btn-group me-2">
            <button type="button" class="btn btn-sm btn-outline-secondary">Share</button>
            <button type="button" class="btn btn-sm btn-outline-secondary">Export</button>
          </div>
          <button type="button" class="btn btn-sm btn-outline-secondary dropdown-toggle">
            <span data-feather="calendar"></span>
            This week
          </button>
        </div>
      </div>

      <div class="container">
IAMSOMEDUMMYTEXT;
  }
  function genFooter()
  {
      echo <<<IAMSOMEDUMMYTEXT

      </main>
  </div>
</div>

    <script src="js/bootstrap.bundle.min.js"></script>

    <script src="https://cdn.jsdelivr.net/npm/feather-icons@4.28.0/dist/feather.min.js" integrity="sha384-uO3SXW5IuS1ZpFPKugNNWqTZRRglnUJK6UAZ/gxOX80nxEkN9NcGZTftn6RzhGWE" crossorigin="anonymous"></script><script src="https://cdn.jsdelivr.net/npm/chart.js@2.9.4/dist/Chart.min.js" integrity="sha384-zNy6FEbO50N+Cg5wap8IKA4M/ZnLJgzc6w2NqACZaK0u0FXfOWRRJOnQtpZun8ha" crossorigin="anonymous"></script><script src="dashboard.js"></script>
  </body>
</html>
IAMSOMEDUMMYTEXT;
  }
  ?>
