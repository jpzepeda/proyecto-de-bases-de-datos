  <?php
  include("constans.php");
include("DBConnection.php");
include_once("preparequery.php");
include("generate.php");
genHeader("localhost", "8093");
  $table=$_COOKIE["table"];

switch ($table) {
    case "prestamo":
        displayList($db, "prestamos", $tablesToJoinPrestamos, $RowsFromVariousTablesPrestamos, $humanRowsPrestamos, 7, "id_prestamo");
        break;
    case "person":
        displayList($db, "personas", $tablesToJoinPersons, $RowsFromVariousTablesPersons, $humanRowsPersons, 9, "id_persona");
        break;
    case "modelo":
        displayList($db, "modelos", $tablesToJoinModels, $RowsFromVariousTablesModels, $humanRowsModels, 11, "id_modelo");
        break;
    case "penalizacion":
        displayList($db, "penalizacion", $tablesToJoinPenalizacion, $RowsFromVariousTablesPenalizacion, $humanRowsPenalizacion, 10, "id_penalizacion");
        break;
    case "equipo":
        displayList($db, "equipos", $tablesToJoinEquipos, $RowsFromVariousTablesEquipos, $humanRowsEquipos, 5, "id_equipo");
        break;
}
function displayList(PDO $db, string $tableName, array $tablesToJoin, array $RowsFromVariousTables, array $humanRows, $stop, string $pk, string $serverDomain="localhost:8093")
{
    $selectsql = generateSelectSql($tableName, $tablesToJoin);
    $selectsql .= whereArguments($_REQUEST);
    $stmt = $db->query($selectsql);
    $n_row = $stmt->fetchColumn();

    $rowsToShow= generateRowsToShow($tableName, $RowsFromVariousTables);
    $selectsql = str_replace("count(*)", $rowsToShow, $selectsql);
    $stmt = $db->query($selectsql);


    if ($n_row>0) {
        if ($n_row>0) {
            ?>

<div class="table-responsive">
<table align="center" cellpadding="5" cellspacing="5" class="table table-striped table-sm">

<?php generateHumanRedable($humanRows); ?>

<?php while ($row = $stmt->fetch()) {
                printRowsOnTable($row, array_keys($RowsFromVariousTables), $stop, $serverDomain, $tableName, $row[$pk], $pk);
            }
        } else {
            echo "<center>No $tableName found in the library by the given parameters </center>" ;
        }
    }
    echo "</table>";
    echo "</div>";
}
genFooter();
  ?>
