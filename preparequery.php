<?php
function whereArguments(array $argv)
{
    $argv=deleteEmptyElements($argv);
    $numberOfElements = count($argv);
    if ($numberOfElements>0) {
        $where=" where ";
    }
    $tablerows=array_keys($argv);
    $searchstrings=array_values($argv);
    for ($i=0;$i<$numberOfElements;$i++) {
        $where.=$tablerows[$i];
        $where.=" LIKE ";
        $where.="\"%{$searchstrings[$i]}%\"";
        if ($i<($numberOfElements-1)) {
            $where.=" and ";
        }
    }
    $where.=";";
    return $where;
}
function deleteEmptyElements(array $argv)
{
    $elementsToDelete=array_keys($argv, "");
    foreach ($elementsToDelete as $currentElement) {
        unset($argv[$currentElement]);
    }
    return $argv;
}
