<?php
//Persons section (normal email entry)
$tablesToJoinPersons = ["estados" => "id_estados"];

$RowsFromVariousTablesPersons =
["id_persona" => "personas",
"nombre" => "personas",
"apellido_paterno" => "personas",
"apellido_materno" => "personas",
"clave_elector" => "personas",
"nombre_estado" => "estados",
"fecha_nacimiento" => "personas",
"fecha_registro" => "personas",
"telefono" => "personas",
"email" => "personas"];

$humanRowsPersons = ["Identificador",
"Nombre",
"Apellido Paterno",
"Apellido Materno",
"Clave de Elector",
"Estado",
"fecha de Nacimiento",
"fecha de Registro",
"Telefono",
"Email"];

//Models Section
/* $tablesToJoinModels = ["sistemas_operativos" => "id_sistema_operativo","fabricantes" => "id_fabricante"]; */
$tablesToJoinModels = ["fabricantes" => "id_fabricante"];

$RowsFromVariousTablesModels =
["id_modelo" => "modelos",
"nombre" => "modelos",
"descripcion" => "modelos",
"n_serie" => "modelos",
"producto" => "modelos",
"procesador" => "modelos",
"ram" => "modelos",
"disco" => "modelos",
"v_bios" => "modelos",
"p_consumo" => "modelos",
"nombre_fabricante" => "fabricantes"];

$humanRowsModels =
["Identificador",
"Modelo",
"Descripcion",
"Numero de serie",
"Producto",
"Procesador",
"Ram",
"Disco",
"Version de bios",
"Consumo en Watts",
"Fabricante"];

//Equipos
$tablesToJoinEquipos = ["modelos" => "id_modelo","estado_de_equipo" => "id_estado_de_equipo"];

$RowsFromVariousTablesEquipos =
["id_equipo" => "equipos",
"precio_estimado" => "equipos",
"id_modelo" => "equipos",
"nombre" => "modelos",
"descripcion" => "estado_de_equipo"];

$humanRowsEquipos = ["Identificador",
"Precio estimado",
"Num. Modelo",
"Modelo",
"Estado"];

//Prestamos
$tablesToJoinPrestamos = ["equipos" => "id_equipo","usuarios" => "id_usuario","modelos" => "id_modelo","personas" => "id_persona"];

$RowsFromVariousTablesPrestamos =
["id_prestamo" => "prestamos",
"id_equipo" => "equipos",
"nombre" => "modelos",
"nombre" => "personas",
"apellido_paterno" => "personas",
"apellido_materno" => "personas",
"fecha_salida" => "prestamos",
"fecha_entrada" => "prestamos"];

$humanRowsPrestamos = ["Identificador prestamo",
"Identificador de equipo",
"Modelo",
"Nombre",
"Apellido P",
"Apellido M",
"Fecha Salida",
"Fecha Entrada",
];

//Penalización
$tablesToJoinPenalizacion = ["donaciones" => "id_donacion","usuarios" => "id_usuario","personas" => "id_persona", "prestamos" => "id_prestamo"];

$RowsFromVariousTablesPenalizacion =
["id_penalizacion" => "penalizacion",
"id_donacion" => "donaciones",
"id_equipo" => "donaciones",
"id_prestamo" => "prestamos",
"monto_donado" => "penalizacion",
"nombre" => "personas",
"apellido_paterno" => "personas",
"apellido_materno" => "personas"];

$humanRowsPenalizacion = ["Identificador penalizacion",
"Identificador de donación",
"Identificador de ejemplar",
"Identificador de prestamo",
"Monto donado",
"Nombre",
"Apellido P",
"Apellido M",
];
