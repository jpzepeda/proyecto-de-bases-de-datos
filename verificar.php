<?php
function verify_login(PDO $db)
{
    //Trabajador
    $sqlQuery = "select trabajadores.id_trabajador from trabajadores inner join personas using (id_persona) where nombre like \"%{$_COOKIE["name_login"]}%\";";
    $stmt = $db->query($sqlQuery);
    $_id_trabajador = $stmt->fetchColumn();

    $stmt = $db->prepare("SELECT * FROM trabajadores WHERE id_trabajador = ?");
    $stmt->execute([$_id_trabajador[0]]);
    $user = $stmt->fetch();
    if ($user && password_verify($_COOKIE['pass_login'], $user['pass'])) {
        return "valid";
        setcookie("name_login", "{$_COOKIE["name_login"]}", time()+3600, "/", "", 0);
        setcookie("pass_login", "{$_COOKIE["pass_login"]}", time()+3600, "/", "", 0);
    } else {
        return "invalid";
    }
}
